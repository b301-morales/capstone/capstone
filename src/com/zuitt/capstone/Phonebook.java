package com.zuitt.capstone;

import java.util.ArrayList;

public class Phonebook {

    private ArrayList<Contact> contacts;
    public Phonebook(){
        contacts = new ArrayList<>();
    }
    public void addContact (Contact contact){
        contacts.add(contact);
    }

    public void getContacts(){
        if (contacts.isEmpty()){
            System.out.println("----------------------------------------------------------");
            System.out.println("                    No contacts found");
            System.out.println("----------------------------------------------------------");
        }
        else {
            for (Contact contact : contacts) {
                System.out.println("----------------------------------------------------------");
                System.out.println(contact.getName());
                System.out.println("----------------------------------------------------------");
                System.out.println(contact.getName() + " has the following registered numbers:");
                System.out.println(contact.getContactNumber());
                System.out.println(contact.getName() + " has the following registered addresses:");
                System.out.println(contact.getAddress());
                System.out.println("----------------------------------------------------------");
            }
        }
    }


}
