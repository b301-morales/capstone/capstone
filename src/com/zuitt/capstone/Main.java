package com.zuitt.capstone;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        Phonebook newPhonebook = new Phonebook();
        newPhonebook.getContacts();

        // Phonebook creation
        System.out.println("PHONEBOOK CREATION AND 'EMPTY' PHONEBOOK RESULT ^^^");
        System.out.println();
        System.out.println();

        Contact newContact = new Contact();
        newContact.setName("John Doe");
        newContact.setContactNumber("09151234567");
        newContact.setAddress("Varsity HIlls, QC");

        Contact newContact2 = new Contact();
        newContact2.setName("Jane Doe");
        newContact2.setContactNumber("09157654321");
        newContact2.setAddress("Loyola Heights, QC");

        newPhonebook.addContact(newContact);
        newPhonebook.getContacts();
        System.out.println("1 CONTACT ADDED TO PHONEBOOK RESULT ^^^");
        System.out.println();
        System.out.println();

        newPhonebook.addContact(newContact2);
        newPhonebook.getContacts();
        System.out.println("2 CONTACTS ADDED TO PHONEBOOK RESULT ^^^");
        System.out.println();
        System.out.println();

    }
}
