package com.zuitt.capstone;

public class Contact {

    private String name;
    private String contactNumber;
    private String address;


    public Contact(){
    }
    public Contact(String name){
        this.name = name;
    }

    public Contact(String name, String contactNumber){
        this.name = name;
        this.contactNumber = contactNumber;
    }

    public Contact(String name, String contactNumber, String address){
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }

    // setters

    public void setName(String name){
        this.name = name;
    }
    public void setContactNumber(String contactNumber){
        this.contactNumber = contactNumber;
    }
    public void setAddress(String address){
        this.address = address;
    }

    public void editContact(String name, String contactNumber, String address){
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }

    // getters

    public String getName(){
        return name;
    }
    public String getContactNumber(){
        return contactNumber;
    }
    public String getAddress(){
        return address;
    }

    public void getContact(){
        System.out.println("-----------------------------------");
        System.out.println("Name: " + name);
        System.out.println("Number: " + contactNumber);
        System.out.println("Address: " + address);
        System.out.println("-----------------------------------");
    }
}
